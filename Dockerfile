FROM		nginx:1.7
MAINTAINER	Victor M. Varela <v.varela@neartechnologies.com>

EXPOSE		80 443

ENV		RPROXY_SERVERS			DEFAULT

ENV		RPROXY_DEFAULT_SERVER_NAME	_
ENV		RPROXY_DEFAULT_HTTPS		no
ENV		RPROXY_DEFAULT_LOCATIONS	ROOT
ENV		RPROXY_DEFAULT_ROOT		/
ENV		RPROXY_DEFAULT_ROOT_PATH	/usr/share/nginx/html
ENV		RPROXY_DEFAULT_ROOT_PROXY	no

ADD		./docker-cmd.sh			/
ADD		cert.pem			/etc/nginx/conf.d/cert.pem
ADD		cert.key			/etc/nginx/conf.d/cert.key

CMD		["/docker-cmd.sh"]

