# rproxy

This image is based on the official [`nginx`](https://registry.hub.docker.com/_/nginx/)
image.

## Usage

You use environment variables to change your settings.

Some example:

	RPROXY_SERVERS=DEFAULT,DES,WEB,SYNC,XXX
	RPROXY_DES_SERVER_NAME=des.example.com
	RPROXY_DES_ROOT_HTTP=192.168.10.1:8080
	RPROXY_WEB_SERVER_NAME=web.example.com
	RPROXY_WEB_HTTPS=yes
	RPROXY_SYNC_SERVER_NAME=sync.example.com
	RPROXY_SYNC_HTTPS=yes
	RPROXY_SYNC_ROOT_HTTP=192.168.10.2:8082
	RPROXY_XXX_SERVER_NAME=vuv.mob.neartechnologies.com
	RPROXY_XXX_HTTPS=yes
	RPROXY_XXX_LOCATIONS=ROOT,APP
	RPROXY_XXX_ROOT_HTTP=192.168.10.3:8080
	RPROXY_XXX_APP=/my-uwsgi-app
	RPROXY_XXX_APP_UWSGI=192.168.10.3:8080

