#!/bin/bash

_NGINX_UPSTREAM_SERVERS=""

nginx_add_location () {
	local _LOCATION="$1" _PATH="$2" _UWSGI="$3" _HTTP="$4" _PROXY="$5" _SERVER="$6" _UPSTREAM="" _BASE="$7"
	echo "	location $_LOCATION {"
	case "$_PROXY" in
	'no')		echo "		root ${_PATH};"
			echo "		index index.html index.htm;"
			;;
	'http')		echo "
		add_header 'Access-Control-Allow-Origin' '*';
		proxy_set_header Host \$host;
		proxy_set_header X-Real-IP \$remote_addr;
		proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
		proxy_set_header  X-Forwarded-Proto http;
		proxy_redirect off;
		proxy_pass http://${_SERVER}_${_BASE};
		proxy_send_timeout 6000;
	        if (\$request_filename ~* ^.+.css\$) {
        	        add_header Content-Type text/css;
                	expires 5d;
	        }

"
			_UPSTREAM=$_HTTP
			;;
	'uwsgi')	echo "
		include uwsgi_params;
		uwsgi_param SCRIPT_NAME ${_LOCATION};
		uwsgi_modifier1 30;
		uwsgi_pass ${_SERVER}_${_BASE};
"
			_UPSTREAM=$_UWSGI
			;;
	esac
	echo "	}"
	echo ""

	if [ x"$_UPSTREAM" != "x" ]
	then
		_NGINX_UPSTREAM_SERVERS="${_NGINX_UPSTREAM_SERVERS}
upstream ${_SERVER}_${_BASE} {
	server ${_UPSTREAM} max_fails=3 fail_timeout=30s;
}

"
	fi
	
}

nginx_listen_name () {
	local _NAME="$1" _PORT="$2" _DEFAULT=""
	if [ x"$_NAME" == "x_" ]
	then
		_DEFAULT="default_server"	
	fi
	echo "	listen ${_PORT} ${_DEFAULT};"
	echo "	server_name ${_NAME};"
}

nginx_ssl_on () {
	echo "
	ssl                  on;
	ssl_certificate      conf.d/cert.pem;
	ssl_certificate_key  conf.d/cert.key;

	ssl_session_timeout  5m;

	ssl_protocols  SSLv2 SSLv3 TLSv1;
	ssl_ciphers  ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP;
	ssl_prefer_server_ciphers   on;"
}


nginx_add_server () {
	local _SERVER_NAME="$1" _HTTPS="$2" _LOCATIONS="$3" _SERVER="$4" location="" IFS=" "

	echo ""
	echo "server {"
	nginx_listen_name $_SERVER_NAME 80
	if [ x"$_HTTPS" == "xyes" ]
	then
		echo "	return 301 https://\$host\$request_uri;"
		echo "}"
		echo ""
		echo "server {"
		nginx_listen_name $_SERVER_NAME 443
	fi
	echo "	client_max_body_size 24000M;"
	echo ""
	if [ x"$_HTTPS" == "xyes" ]
	then
		nginx_ssl_on
	fi
	echo ""

	for location in ${_LOCATIONS}
	do
		_LOCATION=$(eval "echo \$$(echo RPROXY_${server}_${location})")
		_LOCATION=${_LOCATION:-"/"}
		_PATH=$(eval "echo \$$(echo RPROXY_${server}_${location}_PATH)")
		_PATH=${_PATH:-"/usr/share/nginx/html"}
		_UWSGI=$(eval "echo \$$(echo RPROXY_${server}_${location}_UWSGI)")
		_HTTP=$(eval "echo \$$(echo RPROXY_${server}_${location}_HTTP)")
		_PROXY=$(eval "echo \$$(echo RPROXY_${server}_${location}_PROXY)")
		if [ x"$_PROXY" == "x" ]
		then
			if [ x"$_UWSGI" != "x" ]
			then
				_PROXY="uwsgi"
			elif [ x"$_HTTP" != "x" ]
			then
				_PROXY="http"
			else
				_PROXY="no"
			fi
		fi
		nginx_add_location "$_LOCATION" "$_PATH" "$_UWSGI" "$_HTTP" "$_PROXY" "$_SERVER" "$location"
	done
	echo ""
	echo "}"
	echo ""
}

nginx_config () {
	local _SERVER_NAME="" _HTTPS="" _LOCATIONS="" server="" IFS=","
	for server in $RPROXY_SERVERS
	do
		if [ x"$server" != "x" ]
		then
			_SERVER_NAME=$(eval "echo \$$(echo RPROXY_${server}_SERVER_NAME)")
			_SERVER_NAME=${_SERVER_NAME:-"localhost"}
			_HTTPS=$(eval "echo \$$(echo RPROXY_${server}_HTTPS)")
			_HTTPS=${_HTTPS:-"no"}
			_LOCATIONS=$(eval "echo \$$(echo RPROXY_${server}_LOCATIONS)")
			_LOCATIONS=${_LOCATIONS:-"ROOT"}
			nginx_add_server "$_SERVER_NAME" "$_HTTPS" "$_LOCATIONS" "$server"
		fi
	done
	echo $_NGINX_UPSTREAM_SERVERS
}

nginx_config > /etc/nginx/conf.d/default.conf

exec nginx -g "daemon off;"

